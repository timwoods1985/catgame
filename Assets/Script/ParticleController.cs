﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    struct ParticleSystemInstance
    {
        public GameObject Object;

        public float Timestamp;
    };

    public static ParticleController Instance = null;

    public static ParticleController GetInstance()
    {
        return Instance;
    }

    public GameObject ExplosionTemplate;

    public float ExplosionLifetime;

    public void AddExplosion(Vector3 Location)
    {
        ParticleSystemInstance NewInstance = new ParticleSystemInstance();

        NewInstance.Object = GameObject.Instantiate(ExplosionTemplate, Location, Quaternion.identity);
        NewInstance.Timestamp = Time.time;
        
        ParticleInstances.Add(NewInstance);
    }

    void Update()
    {
        float CurrentTime = Time.time;

        for (int Index = ParticleInstances.Count - 1; Index >= 0; Index--)
        {
            if (ParticleInstances[Index].Timestamp + ExplosionLifetime < CurrentTime)
            {
                GameObject.Destroy(ParticleInstances[Index].Object);
                ParticleInstances.RemoveAt(Index);
            }
        }
    }

    void Awake()
    {
        Instance = this;
    }

    List<ParticleSystemInstance> ParticleInstances = new List<ParticleSystemInstance>();
}
