﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameScore : MonoBehaviour
{
    public static CatGameScore Instance = null;

    public UnityEngine.UI.Text SavedScoreComponent;
    public UnityEngine.UI.Text LostScoreComponent;

    public static CatGameScore GetInstance()
    {
        return Instance;
    }

    void Awake()
    {
        Instance = this;

    }

    void Start()
    {
        Saved = 0;
        Lost = 0;
    }

    int Saved;
    int Lost;

    public int GetSaved()
    {
        return Saved;
    }

    public int GetLost()
    {
        return Lost;
    }

    public void ItemSaved(int Points)
    {
        Saved++;
        UpdateText();
    }

    public void ItemLost(int Points)
    {
        Lost++;
        UpdateText();
    }

    public void UpdateText()
    {
        if (SavedScoreComponent != null)
        {
            SavedScoreComponent.text = Saved.ToString();
        }

        if (LostScoreComponent != null)
        {
            LostScoreComponent.text = Lost.ToString();
        }
    }
}
