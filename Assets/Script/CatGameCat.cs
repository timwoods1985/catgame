﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameCat : MonoBehaviour
{
    public AudioSource MeowSound;

    enum CatAnimationState
    {
        Jumping,
        Idle,
        Walking,
    };

    enum Difficulty
    {
        Easy,
        Medium,
        Hard,
    };
    
    Difficulty CurrentDifficulty;

    Dictionary<CatGameItemSpawnPoint, float> RecentUsedPositions = new Dictionary<CatGameItemSpawnPoint, float>();

    Animator AnimatorComponent;
    CatAnimationState CurrentAnimationState = CatAnimationState.Idle;
    CatGameItemSpawnPoint CurrentMoveTarget = null;
    CatGameItemSpawnPoint PreviousMoveTarget = null;
    float LastMoveTimestamp = 0;
    Vector3 LastMoveStartPosition;
    bool Walking = false;
    float CurrentMoveCooldown;

    // maybe move to a definition
    public float MoveCooldown = 5;
    public float MoveDuration = 2;
    
    void Awake()
    {
        AnimatorComponent = gameObject.GetComponent<Animator>();

        CurrentDifficulty = Difficulty.Easy;
        CurrentMoveCooldown = MoveCooldown;
    }

    public void IncreaseDifficulty()
    {
        int DifficultyValue = (int)CurrentDifficulty;

        DifficultyValue++;

        if (DifficultyValue > (int)Difficulty.Hard)
        {
            DifficultyValue = (int)Difficulty.Hard;
        }

        SetDifficulty((Difficulty)DifficultyValue);
    }

    void SetDifficulty(Difficulty NewDifficulty)
    {
        if (CurrentDifficulty != NewDifficulty)
        {
            CurrentDifficulty = NewDifficulty;

            switch (NewDifficulty)
            {
                case Difficulty.Easy:
                    {
                        CurrentMoveCooldown = MoveCooldown;
                    }
                    break;

                case Difficulty.Medium:
                    {
                        CurrentMoveCooldown -= 1.0f;
                    }
                    break;

                case Difficulty.Hard:
                    {
                        CurrentMoveCooldown -= 1.0f;
                    }
                    break;
            }
        }

        if (CurrentMoveCooldown < 1.0f)
        {
            CurrentMoveCooldown = 1.0f;
        }
    }
    
    public bool IsCollidable()
    {
        //return CurrentAnimationState != CatAnimationState.Jumping;

        return Walking || CurrentMoveTarget == null;
    }

    void ChangeAnimationState(CatAnimationState AnimationState)
    {
        //if (CurrentAnimationState != AnimationState)
        {
            CurrentAnimationState = AnimationState;

            OnAnimationStateChanged();
        }
    }

    void OnAnimationStateChanged()
    {
        if (AnimatorComponent != null)
        {
            AnimatorComponent.SetBool("Idle", false);
            AnimatorComponent.SetBool("Jump", false);
            AnimatorComponent.SetBool("Walk", false);

            switch (CurrentAnimationState)
            {
                case CatAnimationState.Idle:
                    {
                        AnimatorComponent.SetBool("Idle", true);
                    }
                    break;

                case CatAnimationState.Jumping:
                    {
                        AnimatorComponent.SetBool("Jump", true);
                    }
                    break;

                case CatAnimationState.Walking:
                    {
                        AnimatorComponent.SetBool("Walk", true);
                    }
                    break;
            }
        }
    }

    void UpdateAnimationState()
    {
        if (Walking)
        {
            ChangeAnimationState(CatAnimationState.Walking);
        }
        else if (CurrentMoveTarget != null)
        {
            // must be jumping
            ChangeAnimationState(CatAnimationState.Jumping);
        }
        else
        {
            ChangeAnimationState(CatAnimationState.Idle);
        }

        SpriteRenderer SpriteComponent = gameObject.GetComponent<SpriteRenderer>();

        if (SpriteComponent != null)
        {
            Vector3 Direction = new Vector3(1, 0, 0);

            if (CurrentMoveTarget != null)
            {
                Direction = CurrentMoveTarget.transform.position - transform.position;
            }

            if (Direction.x > 0)
            {
                SpriteComponent.flipX = false;
            }
            else
            {
                SpriteComponent.flipX = true;
            }
        }
    }

    int CompareCatJumpTarget(CatGameItemSpawnPoint A, CatGameItemSpawnPoint B)
    {
        float TimeA = 0.0f;
        float TimeB = 0.0f;
        
        if (RecentUsedPositions.ContainsKey(A))
        {
            TimeA = RecentUsedPositions[A];
        }

        if (RecentUsedPositions.ContainsKey(B))
        {
            TimeB = RecentUsedPositions[B];
        }

        float TimeSinceA = Time.time - TimeA;
        float TimeSinceB = Time.time - TimeB;

        return TimeSinceB.CompareTo(TimeSinceA);
    }

    // TODO: sort by recent cat usage time
    CatGameItemSpawnPoint GetNewJumpLocation()
    {
        CatGameItemSpawnPoint Result = null;
        List<CatGameItemSpawnPoint> PossibleJumpTargets = new List<CatGameItemSpawnPoint>();

        int NumSpawnPoints = CatGameLevel.GetInstance().GetNumSpawnPoints();

        for (int Index = 0; Index < NumSpawnPoints; Index++)
        {
            PossibleJumpTargets.Add(CatGameLevel.GetInstance().GetSpawnPoint(Index));
        }

        if (PossibleJumpTargets.Count > 1)
        {
            PossibleJumpTargets.Remove(PreviousMoveTarget);
        }

        if (PossibleJumpTargets.Count == 0)
        {
            UnityEngine.Debug.Log("cat has no jump locations available");
            return null;
        }

        PossibleJumpTargets.Sort(CompareCatJumpTarget);

        switch (CurrentDifficulty)
        {
            case Difficulty.Easy:
                {
                    int RandomIndex = UnityEngine.Random.Range(0, PossibleJumpTargets.Count);
                    Result = PossibleJumpTargets[RandomIndex];
                }
                break;

            case Difficulty.Medium:
                {
                    int HalfCount = PossibleJumpTargets.Count / 2;
                    int RandomIndex = UnityEngine.Random.Range(0, HalfCount);
                    Result = PossibleJumpTargets[RandomIndex];
                }
                break;

            case Difficulty.Hard:
                {
                    Result = PossibleJumpTargets[0];
                }
                break;
        }
        
        return Result;
    }

    void Start()
    {
        CatGameLevel.GetInstance().AddCat(this);
        LastMoveTimestamp = Time.time;
    }
    
    void Update()
    {
        float CurrentTime = Time.time;
        float CurrentMoveDuration = CurrentTime - LastMoveTimestamp;

        if (CurrentMoveTarget != null)
        {
            float TimeAlongJump = CurrentMoveDuration / MoveDuration;

            Vector3 CurrentPosition = Walking
                ? GetWalkPosition(LastMoveStartPosition, CurrentMoveTarget.transform.position, TimeAlongJump)
                : GetJumpPosition(LastMoveStartPosition, CurrentMoveTarget.transform.position, TimeAlongJump);

            transform.position = CurrentPosition;

            if (TimeAlongJump >= 1)
            {
                PreviousMoveTarget = CurrentMoveTarget;

                if (CurrentMoveTarget.WalkableNeighbours.Count > 0)
                {
                    int RandomChoice = UnityEngine.Random.Range(0, 100);

                    // TODO: reduce 50 every time a walk is chosen
                    // increase back to 50 when a jump is chosen
                    if (RandomChoice > 50)
                    {
                        // don't walk
                        CurrentMoveTarget = null;
                        Walking = false;
                    }
                    else
                    {
                        int RandomIndex = UnityEngine.Random.Range(0, CurrentMoveTarget.WalkableNeighbours.Count);

                        CurrentMoveTarget = CurrentMoveTarget.WalkableNeighbours[RandomIndex];
                        LastMoveStartPosition = transform.position;
                        LastMoveTimestamp = CurrentTime;
                        Walking = true;
                    }
                }
                else
                {
                    CurrentMoveTarget = null;
                    Walking = false;
                }

                CatGameUtil.ManualCollision(this);
            }
        }

        if (CurrentMoveTarget == null && (CurrentTime >= LastMoveTimestamp + MoveDuration + CurrentMoveCooldown))
        {
            CurrentMoveTarget = GetNewJumpLocation();

            if (CurrentMoveTarget != null)
            {
                Meow();

                LastMoveStartPosition = transform.position;
                LastMoveTimestamp = CurrentTime;

                if (RecentUsedPositions.ContainsKey(CurrentMoveTarget))
                {
                    RecentUsedPositions[CurrentMoveTarget] = CurrentTime;
                }
                else
                {
                    RecentUsedPositions.Add(CurrentMoveTarget, CurrentTime);
                }
            }
        }
        
        UpdateAnimationState();
    }

    public void Meow()
    {
        if (MeowSound != null)
        {
            MeowSound.Play();
        }
    }

    float Lerp(float Min, float Max, float Time)
    {
        return Min + (Max - Min) * Time;
    }
    
    Vector3 GetJumpPosition(Vector3 Start, Vector3 End, float Time)
    {
        float X = Lerp(Start.x, End.x, Time);
        float Y = Lerp(Start.y, End.y, Time);
        float Z = Lerp(Start.z, End.z, Time);

        float HeightRange = -1.0f * Mathf.Abs(End.x - Start.x);

        HeightRange = Mathf.Max(HeightRange, -3.0f);

        float HeightScale = (Time - 0.5f) * 2.0f;
        float HeightOffset = (1.0f - (HeightScale * HeightScale)) * HeightRange;

        return new Vector3(X, Y - HeightOffset, Z);
    }

    Vector3 GetWalkPosition(Vector3 Start, Vector3 End, float Time)
    {
        return Vector3.Lerp(Start, End, Time);
    }
}
