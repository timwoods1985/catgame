﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScene : MonoBehaviour
{
    public void LoadLevel(string path)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(path);
    }
}
