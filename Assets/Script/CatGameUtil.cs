﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameUtil : MonoBehaviour
{
    static float Lerp(float Min, float Max, float Time)
    {
        return Min + (Max - Min) * Time;
    }

    public static Vector3 GetJumpPosition(Vector3 Start, Vector3 End, float Time)
    {
        float X = Lerp(Start.x, End.x, Time);
        float Y = Lerp(Start.y, End.y, Time);
        float Z = Lerp(Start.z, End.z, Time);

        float Length = Mathf.Abs(End.x - Start.x);

        float HeightRange = -1.0f * Length;
        float HeightScale = (Time - 0.5f) * 2.0f;
        float HeightOffset = (1.0f - (HeightScale * HeightScale)) * HeightRange;

        return new Vector3(X, Y - HeightOffset, Z);
    }

    public static void ManualCollision(CatGameCat Cat)
    {
        BoxCollider ColliderComponent = Cat.gameObject.GetComponent<BoxCollider>();

        if (ColliderComponent != null)
        {
            Collider[] AllCollisions = UnityEngine.Physics.OverlapBox(ColliderComponent.transform.position, ColliderComponent.size);

            for (int Index = 0; Index < AllCollisions.Length; Index++)
            {
                Collider CurrentCollision = AllCollisions[Index];

                CatGameItem Item = CurrentCollision.gameObject.GetComponent<CatGameItem>();

                if (Item != null)
                {
                    Item.CollideWith(Cat);
                }
            }
        }
    }

    public static void PerformCollisionDetection()
    {
        for (int CatIndex = 0; CatIndex < CatGameLevel.GetInstance().GetNumCats(); CatIndex++)
        {
            ManualCollision(CatGameLevel.GetInstance().GetCat(CatIndex));
        }
    }
}
