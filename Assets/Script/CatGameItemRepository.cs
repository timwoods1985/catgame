﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CatGameItemRepository", order = 1)]
public class CatGameItemRepository : ScriptableObject
{
    public CatGameItemDefinition GetRandomDefinition()
    {
        CatGameItemDefinition Result = null;

        if (AllDefinitions.Count > 0)
        {
            int RandomIndex = UnityEngine.Random.Range(0, AllDefinitions.Count);

            if (RandomIndex < AllDefinitions.Count)
            {
                Result = AllDefinitions[RandomIndex];
            }
        }

        return Result;
    }

    public List<CatGameItemDefinition> AllDefinitions = new List<CatGameItemDefinition>();
}
