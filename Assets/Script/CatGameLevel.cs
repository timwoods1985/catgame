﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameLevel : MonoBehaviour
{
    static CatGameLevel Instance;

    public static CatGameLevel GetInstance()
    {
        if (Instance == null)
        {
            UnityEngine.Debug.LogError("Instanced accessed before being set");
        }

        return Instance;
    }



    void Awake()
    {
        Instance = this;

        FindEverything();
    }

    void FindEverything()
    {
        CatGameItemSpawnPoint[] AllSpawnPoints =  GameObject.FindObjectsOfType<CatGameItemSpawnPoint>();

        for (int Index = 0; Index < AllSpawnPoints.Length; Index++)
        {
            AvailableSpawnPoints.Add(AllSpawnPoints[Index]);
        }
    }
	
	public GameObject CreateItem(CatGameItemDefinition Definition, Vector3 Location)
	{
        GameObject Result = null;

        if (Definition != null)
        {
            Result = GameObject.Instantiate(Definition.Prefab, Location, Quaternion.identity);

            if (Result != null)
            {
                CatGameItem ItemComponent = Result.GetComponent<CatGameItem>();

                if (ItemComponent != null)
                {
                    ItemComponent.Definition = Definition;
                    AllItems.Add(ItemComponent);
                }
                else
                {
                    GameObject.Destroy(Result);
                    Result = null;
                    UnityEngine.Debug.LogError("no cat game item on prefab: " + Definition.Name);
                }
            }
        }

        return Result;
	}
    
    public int GetNumSpawnPoints()
    {
		return AvailableSpawnPoints.Count;
    }
	
	public CatGameItemSpawnPoint GetSpawnPoint(int Index)
	{
		return AvailableSpawnPoints[Index];
	}

    public int GetNumItems()
    {
        return AllItems.Count;
    }

    public CatGameItem GetItem(int Index)
    {
        return AllItems[Index];
    }

    public void RemoveItem(CatGameItem Item)
    {
        AllItems.Remove(Item);
    }

    public void AddCat(CatGameCat Cat)
    {
        AllCats.Add(Cat);
    }

    public int GetNumCats()
    {
        return AllCats.Count;
    }

    public CatGameCat GetCat(int Index)
    {
        if (Index >= 0 && Index < AllCats.Count)
        {
            return AllCats[Index];
        }

        return null;
    }

    List<CatGameItem> AllItems = new List<CatGameItem>();
    List<CatGameCat> AllCats = new List<CatGameCat>();
    List<CatGameItemSpawnPoint> AvailableSpawnPoints = new List<CatGameItemSpawnPoint>();
}
