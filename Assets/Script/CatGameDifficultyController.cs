﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameDifficultyController : MonoBehaviour
{
    public float BrokenItemsForGameOver = 10;

    float DifficultyChangeTimestamp;

    GameObject UseSpawnPoint(CatGameItemSpawnPoint SpawnPoint, Vector3 Position)
    {
        GameObject Result = null;
        CatGameItemDefinition RandomDefinition = Repository.GetRandomDefinition();

        if (RandomDefinition != null)
        {
            Result = CatGameLevel.GetInstance().CreateItem(RandomDefinition, Position);

            if (Result != null && SpawnPoint != null)
            {
                CatGameItem Item = Result.GetComponent<CatGameItem>();

                if (Item != null)
                {
                    Item.OnSpawned(SpawnPoint);
                }
            }
        }

        return Result;
    }

    void Start()
    {
        CatGameLevel Level = CatGameLevel.GetInstance();

        if (Level != null)
        {
            int NumSpawnPoints = Level.GetNumSpawnPoints();

            for (int Index = 0; Index < NumSpawnPoints - 1; Index++)
            {
                CatGameItemSpawnPoint SpawnPoint = Level.GetSpawnPoint(Index);

                if (SpawnPoint != null && !SpawnPoint.IsOccupied())
                {
                    UseSpawnPoint(SpawnPoint, SpawnPoint.transform.position);
                }
            }
        }

        DifficultyChangeTimestamp = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (CatGameScore.GetInstance().GetLost() >= BrokenItemsForGameOver)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("FailMenu");
        }

        int NumItems = CatGameLevel.GetInstance().GetNumItems();

        if (NumItems < 2)
        {
            SpawnExtraItem();
        }

        float CurrentTime = Time.time;
        float TimeSinceDifficultyIncrease = CurrentTime - DifficultyChangeTimestamp;

        if (TimeSinceDifficultyIncrease > 20.0f)
        {
            DifficultyChangeTimestamp = CurrentTime;

            for (int Index = 0; Index < CatGameLevel.GetInstance().GetNumCats(); Index++)
            {
                CatGameLevel.GetInstance().GetCat(Index).IncreaseDifficulty();
            }
        }

        // hack to avoid tracking down a bug...
        if (NumItems == 0)
        {
            if (CatGameScore.GetInstance().GetLost() >= BrokenItemsForGameOver)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("FailMenu");
            }
            else
            {
                int NumSpawnPoints = CatGameLevel.GetInstance().GetNumSpawnPoints();

                for (int Index = 0; Index < NumSpawnPoints; Index++)
                {
                    CatGameLevel.GetInstance().GetSpawnPoint(Index).SetOccupied(false);
                }

                for (int Index = 0; Index < 3; Index++)
                {
                    SpawnExtraItem();
                }
            }
        }
    }

    void SpawnExtraItem()
    {
        CatGameItemSpawnPoint SpawnPoint = CatGameItemSpawnPoint.FindOnUnoccupiedSpot();

        if (SpawnPoint != null)
        {
            GameObject NewItem = UseSpawnPoint(null, Vector3.zero);

            CatGameItem Item = NewItem.GetComponent<CatGameItem>();

            if (Item != null)
            {
                Item.OnItemSaved();
            }
        }
    }

    public CatGameItemRepository Repository;
}
