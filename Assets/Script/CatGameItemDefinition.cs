﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CatGameItem", order = 1)]
public class CatGameItemDefinition : ScriptableObject
{
    public string Name;

    public GameObject Prefab;

    public int Points;
}
