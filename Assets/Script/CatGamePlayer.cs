﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGamePlayer : MonoBehaviour
{
    enum PlayerDirection
    {
        Left,
        Right,
        Idle,
    };
    
    Animator AnimatorComponent;

    public AudioSource WalkClip;

    public float LowerPositionLimit = -100;

    public float UpperPositionLimit = 100;

    public float PlayerSpeed = 1;

    PlayerDirection CurrentDirection = PlayerDirection.Idle;

    Vector3 CurrentPosition;

    Transform CurrentTransform = null;

    void Awake()
    {
        AnimatorComponent = gameObject.GetComponent<Animator>();
        CurrentPosition = transform.position;
        CurrentTransform = transform;
    }

    void SetAnimationState(PlayerDirection State)
    {
        if (CurrentDirection != State)
        {
            CurrentDirection = State;
            SpriteRenderer SpriteComponent = gameObject.GetComponent<SpriteRenderer>();
            
            switch (State)
            {
                case PlayerDirection.Idle:
                    {
                        WalkClip.Stop();

                        if (AnimatorComponent != null)
                        {
                            AnimatorComponent.SetBool("Idle", true);
                            AnimatorComponent.SetBool("Walk", false);
                        }
                    }
                    break;

                case PlayerDirection.Left:
                    {
                        WalkClip.Play();

                        if (AnimatorComponent != null)
                        {
                            AnimatorComponent.SetBool("Idle", false);
                            AnimatorComponent.SetBool("Walk", true);
                        }
                    }
                    break;

                case PlayerDirection.Right:
                    {
                        WalkClip.Play();

                        if (AnimatorComponent != null)
                        {
                            AnimatorComponent.SetBool("Idle", false);
                            AnimatorComponent.SetBool("Walk", true);
                        }
                    }
                    break;
            }
        }
    }
    
    void Update()
    {
        Vector3 Direction = new Vector3(0, 0, 0);
        SpriteRenderer SpriteComponent = gameObject.GetComponent<SpriteRenderer>();

        float HorizontalInputAmount = Input.GetAxis("Horizontal");

        if (HorizontalInputAmount > 0)
        {
            Direction.Set(1, 0, 0);
            SetAnimationState(PlayerDirection.Right);

            if (SpriteComponent != null)
            {
                SpriteComponent.flipX = false;
            }
        }
        else if (HorizontalInputAmount < 0)
        {
            Direction.Set(-1, 0, 0);
            SetAnimationState(PlayerDirection.Left);

            if (SpriteComponent != null)
            {
                SpriteComponent.flipX = true;
            }

        }
        else
        {
            Direction.Set(0, 0, 0);
            SetAnimationState(PlayerDirection.Idle);
        }
        
        CurrentPosition += Direction * PlayerSpeed * Time.deltaTime;

        CurrentPosition.x = Mathf.Clamp(CurrentPosition.x, LowerPositionLimit, UpperPositionLimit);

        CurrentTransform.position = CurrentPosition;
    }
}
