﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameAudio : MonoBehaviour
{
    public static CatGameAudio Instance = null;

    public static CatGameAudio GetInstance()
    {
        return Instance;
    }

    void Awake()
    {
        Instance = this;
    }

    public AudioSource AudioComponent;
    public List<AudioClip> BrokenSounds = new List<AudioClip>();

    public void PlayBrokenSound()
    {
        if (BrokenSounds.Count > 0 && AudioComponent != null)
        {
            int RandomIndex = UnityEngine.Random.Range(0, BrokenSounds.Count);

            AudioComponent.clip = BrokenSounds[RandomIndex];
            AudioComponent.Play();
        }
    }

}
