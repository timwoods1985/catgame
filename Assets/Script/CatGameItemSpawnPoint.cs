﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameItemSpawnPoint : MonoBehaviour
{
    public static CatGameItemSpawnPoint FindOnUnoccupiedSpot()
    {
        CatGameItemSpawnPoint Result = null;
        List<CatGameItemSpawnPoint> Candidates = new List<CatGameItemSpawnPoint>();

        int NumSpawnPoints = CatGameLevel.GetInstance().GetNumSpawnPoints();

        for (int Index = 0; Index < NumSpawnPoints; Index++)
        {
            CatGameItemSpawnPoint CurrentPoint = CatGameLevel.GetInstance().GetSpawnPoint(Index);

            if (!CurrentPoint.IsOccupied())
            {
                Candidates.Add(CurrentPoint);
            }
        }

        Candidates.Sort(CompareUnoccupiedSpot);

        if (Candidates.Count > 0)
        {
            int RandomIndex = UnityEngine.Random.Range(0, Candidates.Count);
            Result = Candidates[RandomIndex];
        }

        return Result;
    }

    static int CompareUnoccupiedSpot(CatGameItemSpawnPoint A, CatGameItemSpawnPoint B)
    {
        CatGameCat Cat = CatGameLevel.GetInstance().GetCat(0);

        if (Cat != null)
        {
            Vector3 DistanceFromCatA = A.transform.position - Cat.transform.position;
            Vector3 DistanceFromCatB = B.transform.position - Cat.transform.position;

            return DistanceFromCatB.x.CompareTo(DistanceFromCatA.x);
        }

        return 0;
    }

    public bool IsOccupied()
    {
        return AmOccupied;
    }

    public void SetOccupied(bool Occupied)
    {
        AmOccupied = Occupied;
    }

    public int GetNumNeighbours()
    {
        return WalkableNeighbours.Count;
    }

    public CatGameItemSpawnPoint GetNeighbour(int Index)
    {
        return WalkableNeighbours[Index];
    }

    void Awake()
    {
        AmOccupied = false;
    }

    bool AmOccupied;
    public List<CatGameItemSpawnPoint> WalkableNeighbours = new List<CatGameItemSpawnPoint>();
}
