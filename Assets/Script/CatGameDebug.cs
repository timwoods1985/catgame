﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGameDebug : MonoBehaviour
{
    public static CatGameDebug Instance = null;

    public static CatGameDebug GetInstance()
    {
        return Instance;
    }

    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        int OccupiedSlots = 0;
        int UnoccupiedSlots = 0;

        for (int i=0; i < CatGameLevel.GetInstance().GetNumSpawnPoints(); i++)
        {
            if (CatGameLevel.GetInstance().GetSpawnPoint(i).IsOccupied())
            {
                OccupiedSlots++;
            }
            else
            {
                UnoccupiedSlots++;
            }
        }

        SetDebugMessage("SpawnPointOccupyDebug", "Occupied: " + OccupiedSlots + " - Unoccupied: " + UnoccupiedSlots);
    }

    void SetDebugMessage(string Key, string Value)
    {
        if (VisibleDebugMessages.ContainsKey(Key))
        {
            VisibleDebugMessages[Key] = Value;
        }
        else
        {
            VisibleDebugMessages.Add(Key, Value);
        }

        OnMessagesUpdated();
    }

    void OnMessagesUpdated()
    {
        if (TextComponent != null)
        {
            string DisplayText = "";

            foreach (KeyValuePair<string, string> Entry in VisibleDebugMessages)
            {
                DisplayText += Entry.Value + "\n";
            }

            TextComponent.text = DisplayText;
        }
    }

    public UnityEngine.UI.Text TextComponent;

    Dictionary<string, string> VisibleDebugMessages = new Dictionary<string, string>();
}
